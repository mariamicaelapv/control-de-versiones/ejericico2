<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
      public function actionConsulta1a() {
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("count(dorsal) as total_ciclistas")
                ->where ("")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['total_ciclistas'],
                    "titulo" => "Consulta 1 con Active Record",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT count(*) FROM ciclista",
        ]);
    }

    public function actionConsulta1() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(dorsal) as total_ciclistas FROM ciclista',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*)FROM ciclista",
            ]);
    }
     public function actionConsulta2a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("count(dorsal) as media_ciclistas")
                ->where ("nomequipo='Banesto'")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);
        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['media_ciclistas'],
                    "titulo" => "Consulta 2 con Active Record",
                    "enunciado" => "Número de ciclistas que hay del equipo Banesto",
                    "sql" => "SELECT count(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
      public function actionConsulta2() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(dorsal) as media_ciclistas FROM ciclista WHERE nomequipo="Banesto"',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_ciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT count(*) as media_ciclistas FROM ciclista WHERE nomequipo='Banesto'",
            ]);
    }
    
    public function actionConsulta3a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("AVG(edad) as media_edad")
                ->where ("")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['media_edad'],
                    "titulo" => "Consulta 3 con Active Record",
                    "enunciado" => "Edad media de los ciclistas ",
                    "sql" => "SELECT AVG(edad) as media_edad FROM ciclista'",
        ]);
    }
     public function actionConsulta3() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as media_edad FROM ciclista',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>" Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as media_edad FROM CICLISTAS",
            ]);
    }
    public function actionConsulta4a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("AVG(edad) as media_edad")
                ->where ("nomequipo='Banesto'")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['media_edad'],
                    "titulo" => "Consulta 4 con Active Record",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => "SELECT AVG(edad) as media_edad FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta4() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as media_edad FROM ciclista WHERE nomequipo="Banesto"',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>" Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as media_edad FROM ciclistas WHERE nomequipo='Banesto'",
            ]);
    }
               
public function actionConsulta5a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("AVG(edad) as media_edad,nomequipo")
                ->groupBy("nomequipo")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['media_edad','nomequipo'],
                    "titulo" => "Consulta 5 con Active Record",
                    "enunciado" => "La edad media de los ciclistas por cada equipo",
                    "sql" => " select  AVG(edad),nomequipo from ciclista group by nomequipo",
        ]);
    }

 public function actionConsulta5() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'select AVG(edad)as media_edad,nomequipo FROM ciclista GROUP BY nomequipo',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad','nomequipo'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado" => "La edad media de los ciclistas por cada equipo",
            "sql" => " SELECT AVG(edad),nomequipo from ciclista group by nomequipo",
            ]);
    }
    public function actionConsulta6a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo,Count(dorsal) as total_ciclistas")
                ->groupBy("nomequipo")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','total_ciclistas'],
                    "titulo" => "Consulta 6 con Active Record",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => "select nomequipo,count(*)from ciclista group by nomequipo;",
        ]);
    }
    public function actionConsulta6() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'  select nomequipo,count(dorsal) as total_ciclistas from ciclista group by nomequipo',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
         return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','total_ciclistas'],
                    "titulo" => "Consulta 6 con Dao",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => "select nomequipo,count(*)from ciclista group by nomequipo",
        ]);
    }
    public function actionConsulta7a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("Count(dorsal) as puertos_totales")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['puertos_totales'],
                    "titulo" => "Consulta 7 con Active Record",
                    "enunciado" => "El número total de puertos",
                    "sql" => "SELECT COUNT(*)as puertos_totales FROM puerto",
        ]);
    }
    public function actionConsulta7() {
        $dataProvider = new SqlDataProvider([
            'sql'=>' SELECT COUNT(dorsal) as puertos_totales FROM puerto',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
         return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['puertos_totales'],
                    "titulo" => "Consulta 7 DAO",
                    "enunciado" => "El número total de puertos",
                    "sql" => "SELECT COUNT(*)as puertos_totales FROM puerto",
        ]);
    }
     public function actionConsulta8a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("Count(dorsal) as puertos_totales")
                ->where("altura>1500")
                ->distinct(),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['puertos_totales'],
                    "titulo" => "Consulta 8 con Active Record",
                    "enunciado" => "El número total de puertos mayores de 1500",
                    "sql" => "SELECT COUNT(*)as puertos_totales FROM puerto WHERE altura>1500",
        ]);
    }
    public function actionConsulta8() {
        $dataProvider = new SqlDataProvider([
            'sql'=>' SELECT COUNT(dorsal) as puertos_totales FROM puerto WHERE altura<1500',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
          return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['puertos_totales'],
                    "titulo" => "Consulta 8 DAO",
                    "enunciado" => "El número total de puertos mayores de 1500",
                    "sql" => "SELECT COUNT(*)as puertos_totales FROM puerto WHERE altura>1500",
        ]);
    }
    public function actionConsulta9a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo")
                ->groupBy("nomequipo")
                ->HAVING("count(*)>4"),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 9 con Active Record",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql" =>"select nomequipo,count(*) from ciclista group by nomequipo having count(*)>4",
        ]);
    }
    public function actionConsulta9() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nomequipo,count(*) from ciclista group by nomequipo having count(*)>4',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
          return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 9 DAO",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql"=>"select nomequipo,count(*) from ciclista group by nomequipo having count(*)>4"
        ]);
    }
    public function actionConsulta10a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo")
               ->where("edad BETWEEN 28 and 32")
                ->groupBy("nomequipo")
                ->HAVING("count(*)>4"),
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 10 con Active Record",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" =>"select nomequipo,count(*) from ciclista where edad between 28 and 32 group by nomequipo having count(*)>4",
        ]);
    }
     public function actionConsulta10() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nomequipo,count(*) from ciclista WHERE edad BETWEEN 28 AND 32 group by nomequipo having count(*)>4',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
          return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 10 DAO",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" =>"select nomequipo,count(*) from ciclista where edad between 28 and 32 group by nomequipo having count(*)>4",
        ]);
    }
     public function actionConsulta11a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal,count(*)")
               ->groupBy("dorsal"),
               
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                    "titulo" => "Consulta 11 con Active Record",
                    "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" =>"SELECT dorsal,count(*) from etapa group by dorsal",
        ]);
    }
     public function actionConsulta11() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal,count(*) from etapa group by dorsal',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
          return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                   "titulo" => "Consulta 11 con DAO",
                    "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" =>"SELECT dorsal,count(*) from etapa group by dorsal",
        ]);
    }
     public function actionConsulta12a() {
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal,count(*)")
               ->groupBy("dorsal")
           ->HAVING("count(*)>1"),
               
            'pagination' => [
                'pageSize' => 5,
            ]
                ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                    "titulo" => "Consulta 12 con Active Record",
                    "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" =>"SELECT dorsal,count(*) from etapa group by dorsal HAVING COUNT(*)>1",
        ]);
    }
    public function actionConsulta12() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal,count(*) from etapa group by dorsal HAVING COUNT(*)>1',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
          return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                   "titulo" => "Consulta 12 con DAO",
                    "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" =>"SELECT dorsal,count(*) from etapa group by dorsal HAVING COUNT(*)>1",
        ]);
    }
}
